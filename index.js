const fetch = require('node-fetch');

class QStream {
  constructor(ip) {
    this.ip = ip;
  }

  async getAirQuality() {
    const response = await fetch(`http://${this.ip}/AQI`);
    const json = await response.json();

    return parseInt(json['Value']);
  }

  async getLevel(modeIndex) {
    const response = await fetch(`http://${this.ip}/Levels?Index=${modeIndex}`);
    const json = await response.json();

    return parseInt(json['Value'].substring(0, json['Value'].length - 2));
  }

  async getStatus() {
    const response = await fetch(`http://${this.ip}/Status`);
    const json = await response.json();

    const regex = /TIMER (?<timer>ACTIVE|INACTIVE) (?<schedule>SCHEDULE OFF|(?<schedule_minutes>\d+) MIN) Qanalog (?<qanalog>\d+)% Qset (?<qset>\d+)% Qactual (?<qactual>\d+)% DEMAND CONTROL (?<demand_control>ON|OFF) .*? VALVE (?<valve_status>CLOSED|OPENED)/gm;
    const matches = regex.exec(json['Value']);

    return {
      timer: matches.groups['timer'] === 'ACTIVE',
      schedule: matches.groups['schedule'] !== 'SCHEDULE OFF',
      schedule_minutes: parseInt(matches.groups['schedule_minutes']),
      analog: matches.groups['qanalog'],
      set: parseInt(matches.groups['qset']),
      actual: parseInt(matches.groups['qactual']),
      demand_control: matches.groups['demand_control'] === 'ON',
      valve: matches.groups['valve_status'] !== 'CLOSED',
    };
  }

  async getSchedule(dayIndex) {
    const response = await fetch(`http://${this.ip}/Schedule?Day=${dayIndex}`);
    const json = await response.json();

    const activities = [];
    for (let i = 0; i < json['Value'].length; i++) {
      if (json['Value'][i] === 'N/A') {
        continue;
      }

      const activityData = json['Value'][i].split(' ');
      activities.push({time: activityData[0], intensity: parseInt(activityData[1].substring(0, activityData[1].length - 2))});
    }

    return activities;
  }

  async clearSchedule() {
    const response = await fetch(`http://${this.ip}/ScheduleEnable`, {
      method: 'post',
      body: JSON.stringify({Value: 'off'}),
      headers: {'Content-Type': 'application/json'},
    });

    return response.json();
  }

  async setTimer(minutes, speedPercentage) {
    let valueString = '';
    valueString += `TIMER ${minutes} MIN`;

    if (minutes !== 0) {
      if (speedPercentage.length === 0) {
        throw new Error('Invalid speedPercentage entered');
      }

      valueString += ` ${speedPercentage}% DEMAND CONTROL OFF DAY`;
    }

    const response = await fetch(`http://${this.ip}/Timer`, {
      method: 'post',
      body: JSON.stringify({Value: valueString}),
      headers: {'Content-Type': 'application/json'},
    });

    return response.json();
  }
}

module.exports = QStream;

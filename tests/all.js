const QStream = require('../index');
const {assert} = require('chai');

const QSTREAM_TEST_IP = '192.168.4.1';

describe('QStream', function() {
  describe('#getAirQuality()', function() {
    it('should return the air quality as an integer', async function() {
      const qstream = new QStream(QSTREAM_TEST_IP);
      const quality = await qstream.getAirQuality();

      assert.isNumber(quality, 'Expected a number for quality');
      assert.isAbove(quality, 0, 'Expected quality to be higher than 0');
      assert.isAtMost(quality, 20, 'Expected quality to be lower than 20');
    });
  });

  describe('#getLevel()', function() {
    it('should return the air quality as an integer', async function() {
      const qstream = new QStream(QSTREAM_TEST_IP);
      const level = await qstream.getLevel(0);

      assert.isNumber(level, 'Expected a number for level');
      assert.isAtLeast(level, 0, 'Expected level to be equal or higher than 0');
      assert.isAtMost(level, 20, 'Expected level to be lower than 200');
    });
  });

  describe('#getStatus()', function() {
    it('should successfully parse the result and return an object', async function() {
      const qstream = new QStream(QSTREAM_TEST_IP);
      const status = await qstream.getStatus();

      assert.equal(status.hasOwnProperty('timer'), true, 'Unexpected object received!');
    });
  });

  describe('#getSchedule()', function() {
    it('should successfully return the schedule of day 0', async function() {
      const qstream = new QStream(QSTREAM_TEST_IP);
      const schedule = await qstream.getSchedule(0);

      assert.isArray(schedule, 'Did not get an array of activities');

      if (schedule.length > 0) {
        assert.equal(schedule[0].hasOwnProperty('time'), true, 'Activities received, but not according to format');
      }
    });
  });

  describe('#setTimer()', function() {
    it('should set a timer for 10 minutes at safe maximum speed', async function() {
      const qstream = new QStream(QSTREAM_TEST_IP);
      const res = await qstream.setTimer(10, 100);
    });
  });

  describe('#setTimer()', function() {
    it('should cancel previously set timer', async function() {
      const qstream = new QStream(QSTREAM_TEST_IP);
      const res = await qstream.setTimer(0);
    });
  });

  describe('#clearSchedule()', function() {
    it('should clear all schedules from the device', async function() {
      const qstream = new QStream(QSTREAM_TEST_IP);
      const res = await qstream.clearSchedule();

      assert.isObject(res, 'Expected an object as response');
      assert.equal(res.Value, 'off', 'Expected "off" as result of clearSchedule request');
    });
  });
});
